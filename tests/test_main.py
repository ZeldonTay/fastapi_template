"""Modules import"""
import pytest
import json
from api_template.main import app
from fastapi.testclient import TestClient

"""Testing part"""
@pytest.fixture(scope="session", autouse=True)
def client():
    return TestClient(app)

def test_root_responds(client):
    response = client.get("/")
    assert response.status_code == 200

def test_root_message(client):
    response = client.get("/")
    response =  response.json()
    assert response['message'] == "hello world"

def test_inference_responds(client):
    payload = {"data": {"bmi":"21"}}

    headers = {
        'api_key': 'aicoe_test_token',
        'Content-Type': 'application/json'
    }
    response = client.post("/inference", data=json.dumps(payload), headers=headers)
    assert response.status_code == 200

def test_inference_api_key_absent_return_403(client):
    response = client.post("/inference")
    assert response.status_code == 403

def test_inference_message(client):
    payload = {"data": {"bmi":"21"}}

    headers = {
        'api_key': 'aicoe_test_token',
        'Content-Type': 'application/json'
    }
    response = client.post("/inference", data=json.dumps(payload), headers=headers)
    response = response.json()
    print(response)
    assert response['model'] == "health_score:0.0.1"