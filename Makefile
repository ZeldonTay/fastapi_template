setup:
	python3 -m venv ~/.api_template

install:
	pip3 install -r requirements.txt

test:
	python3 -m pytest -vv --capture=sys tests/*.py

citest:
	python3 -m pytest -vv --capture=sys tests/*.py --alluredir=results

build:
	docker build -t api_template:0.0.1 .

lint:
	pylint --disable=R,C api_template

all: install test build