"""
Data Modeling of the library
"""
from pydantic import BaseModel
from typing import Optional

class DataModel(BaseModel):
    bmi: str
    waterLitre_perday: Optional[str] = "2"
    anyExercise_perweek: Optional[str] = "0"
    sleepingtime_perday: Optional[str] = "8"
