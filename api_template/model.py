"""
Model Inference Library
"""

def inference(bmi:str,
              sleepingtime_perday:str,
              waterLitre_perday:str,
              anyExercise_perweek: str
              ) -> str:
    health_score = 10.0

    bmi = float(bmi)
    waterLitre_perday = float(waterLitre_perday)
    sleepingtime_perday = float(sleepingtime_perday)
    anyExercise_perweek = int(anyExercise_perweek)


    if bmi > 30:
        adj_score = 1
    elif 25 < bmi <= 30:
        adj_score = 2
    elif 22.9 < bmi <= 25:
        adj_score = 3
    elif 20 < bmi <= 22.9:
        adj_score = 4
    elif 18 < bmi <= 20:
        adj_score = 5
    else:
        adj_score = 3


    waterLitre_perday = min(waterLitre_perday, 2.0)
    adj_score = adj_score*(waterLitre_perday/2)
    adj_score =+ anyExercise_perweek*2

    if sleepingtime_perday <= 6:
        adj_score -= 1

    health_score+=adj_score

    return str(health_score)