"""
Utilities of the library
"""
import logging
import pandas as pd
import os
from datetime import datetime
from fastapi import Depends, HTTPException
from starlette.status import HTTP_401_UNAUTHORIZED
from fastapi.security.api_key import APIKeyHeader
from couchbase.cluster import Cluster, ClusterOptions, PasswordAuthenticator, ClusterTimeoutOptions
from couchbase.options import LOCKMODE_WAIT
from couchbase.exceptions import CouchbaseException as cbe
from datetime import timedelta

logger = logging.getLogger("app")
couchhost_uri = os.getenv("CB_HOST")
username = os.getenv("CB_USERNAME")
password = os.getenv("CB_PASSWORD")
bucketname = os.getenv("CB_BUCKETNAME")

#===== Couch Connections =====
def connect_couchbase():
    try:
        # init couchbase connection setup
        cluster = Cluster(couchhost_uri,
                          ClusterOptions(
                              PasswordAuthenticator(username, password),
                              timeout_options = ClusterTimeoutOptions(kv_timeout=timedelta(0, 10, 0)),
                              lockmode=LOCKMODE_WAIT
                          ))
        collection = cluster.bucket(bucketname).default_collection()
    except cbe as e:
        logger.error(e)

# 1. query data from couchbase
# def getQueryResultInList(cb=None, query=""):
#     cb = cb or cluster
#     results = cb.query(query)
#     # the result is in a generator, so generate it to list
#     return [r for r in results]

# 2. sample query
# query = "Select * from teamone_data where user_id='happygolucky';"
# sample result:
# [{'teamone_data': {'user_id': 'happygolucky', 'water_consumed': 5, 'timestamp': '2020-07-01 14:21:40'}},
# {'teamone_data': {'user_id': 'happygolucky', 'water_consumed': 5, 'timestamp': '2020-07-01 14:21:40'}},

# 3a. define a unique key for indexing
def get_id():
    return "_".join(["_id", str(datetime.timestamp(datetime.now()))])

# 3b. insert to couchbase if required
# sample insert code
# doc = {"user_id":"happygolucky", "someotherfield":"xyz"}
# bucket.insert(get_id(),doc)


#===== API Key =====
# api key connection setup
df = pd.read_csv("api_key.csv")
API_KEY = df.loc[::, "api_key"].to_list()
X_API_KEY = APIKeyHeader(name='api_key')

def check_authentication_header(x_api_key: str = Depends(X_API_KEY)):
    """ takes the X-API-Key header and converts it into the matching user object from the database """
    logger.info("api_key: %s", x_api_key)
    # this is where the SQL query for converting the API key into a user_id will go
    if x_api_key in API_KEY:
        # if passes validation check, return user data for API Key
        # future DB query will go here
        # return {
        #     "id": 1234567890,
        #     "companies": [1, ],
        #     "sites": [],
        # }
        return x_api_key
    # else raise 401
    raise HTTPException(
        status_code=HTTP_401_UNAUTHORIZED,
        detail="Invalid API Key",
    )


